package com.ey.system.admin.module.admin.service;

import com.ey.system.admin.module.admin.domain.Demo;
import com.ey.system.admin.module.admin.dto.DemoDTO;

import java.util.List;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:15
 */
public interface IDemoService {

    List<DemoDTO> queryAll(Demo queryCondition);

    void delete(Long id);

    void update(Demo resources);

    DemoDTO create(Demo resources);
}
