package com.ey.system.admin.module.generator.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:08
 */
@Service
@Data
public class GenConfig {

    @Value("${ey-system.generator.author}")
    private String author = "zhuang_ming_nan";

}
