package com.ey.system.admin.module.admin.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:18
 */
@Entity
@Data
@Table(name = "demo")
public class Demo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @NotNull(groups = Update.class)
    private Long id;

    private String name;

    public @interface Update {}
}
