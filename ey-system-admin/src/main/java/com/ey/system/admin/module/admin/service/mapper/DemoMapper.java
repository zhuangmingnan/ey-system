package com.ey.system.admin.module.admin.service.mapper;

import com.ey.system.admin.common.mapper.EntityMapper;
import com.ey.system.admin.module.admin.domain.Demo;
import com.ey.system.admin.module.admin.dto.DemoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:41
 */
@Mapper(componentModel = "spring",uses = {},unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DemoMapper extends EntityMapper<DemoDTO, Demo> {
}
