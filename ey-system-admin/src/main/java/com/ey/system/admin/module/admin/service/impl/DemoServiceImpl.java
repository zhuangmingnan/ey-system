package com.ey.system.admin.module.admin.service.impl;

import com.ey.system.admin.module.admin.dao.DemoRepository;
import com.ey.system.admin.module.admin.domain.Demo;
import com.ey.system.admin.module.admin.dto.DemoDTO;
import com.ey.system.admin.module.admin.service.IDemoService;
import com.ey.system.admin.module.admin.service.mapper.DemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:16
 */
@Service
public class DemoServiceImpl implements IDemoService {

    @Autowired
    private DemoRepository demoRepository;
    @Autowired
    private DemoMapper demoMapper;

    @Override
    public List<DemoDTO> queryAll(Demo queryCondition) {
        return demoMapper.toDto(demoRepository.findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long id) {
        demoRepository.deleteById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(Demo resources) {
        Assert.notNull(resources.getId(), "id不能为空");
        demoRepository.save(resources);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public DemoDTO create(Demo resources) {
        return demoMapper.toDto(demoRepository.save(resources));
    }
}
