package com.ey.system.admin.module.generator.controller;

import com.ey.system.admin.module.generator.service.IGenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/28 0:09
 */
@RestController
@RequestMapping("api")
public class GenController {

    @Autowired
    private IGenService iGenService;

    @GetMapping("gen")
    public ResponseEntity gen(String tableName) {
        iGenService.gen(tableName);
        return ResponseEntity.ok("生成代码成功");
    }
}
