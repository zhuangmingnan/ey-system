package com.ey.system.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EySystemAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(EySystemAdminApplication.class, args);
    }

}
