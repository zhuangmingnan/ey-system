package com.ey.system.admin.module.admin.dao;

import com.ey.system.admin.module.admin.domain.Demo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:34
 */
public interface DemoRepository extends JpaRepository<Demo, Long>, JpaSpecificationExecutor {
}
