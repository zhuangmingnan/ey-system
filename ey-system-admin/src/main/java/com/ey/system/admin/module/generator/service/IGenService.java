package com.ey.system.admin.module.generator.service;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/28 0:11
 */
public interface IGenService {

    void gen(String tableName);
}
