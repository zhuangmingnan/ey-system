package com.ey.system.admin.module.admin.controller;

import com.ey.system.admin.module.admin.domain.Demo;
import com.ey.system.admin.module.admin.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:12
 */
@RestController
@RequestMapping("api")
public class DemoController {

    @Autowired
    private IDemoService iDemoService;

    @GetMapping("/demo")
    public ResponseEntity get(Demo queryCondition) {
        return ResponseEntity.ok(iDemoService.queryAll(queryCondition));
    }

    @PostMapping("/demo")
    public ResponseEntity add(@RequestBody Demo resources) {
        return ResponseEntity.ok(iDemoService.create(resources));
    }

    @PutMapping("demo")
    public ResponseEntity update(@RequestBody Demo resources) {
        iDemoService.update(resources);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/demo/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        iDemoService.delete(id);
        return ResponseEntity.ok().build();
    }

}
