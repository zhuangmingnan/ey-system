package com.ey.system.admin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 19:02
 */
@RestController
public class HealthController {

    @GetMapping("/health.do")
    public String health() {
        return "It's ey-system-admin running.";
    }
}
