package com.ey.system.admin.module.admin.dto;

import lombok.Data;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 22:42
 */
@Data
public class DemoDTO {

    private Long id;

    private String name;
}
