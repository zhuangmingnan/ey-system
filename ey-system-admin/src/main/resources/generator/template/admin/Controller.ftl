package com.ey.system.admin.module.admin.controller;

import com.ey.system.admin.module.admin.domain.${tableName?cap_first};
import com.ey.system.admin.module.admin.service.I${tableName?cap_first}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author ${author!"author"}
 * @since ${.now?date} ${.now?time}
 */
@RestController
@RequestMapping("api")
public class ${tableName?cap_first}Controller {

    @Autowired
    private I${tableName?cap_first}Service i${tableName?cap_first}Service;

    @GetMapping("/${tableName}")
    public ResponseEntity get(${tableName?cap_first} queryCondition) {
        return ResponseEntity.ok(i${tableName?cap_first}Service.queryAll(queryCondition));
    }

    @PostMapping("/${tableName}")
    public ResponseEntity add(@RequestBody ${tableName?cap_first} resources) {
        return ResponseEntity.ok(i${tableName?cap_first}Service.create(resources));
    }

    @PutMapping("/${tableName}")
    public ResponseEntity update(@RequestBody ${tableName?cap_first} resources) {
        i${tableName?cap_first}Service.update(resources);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/${tableName}/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        i${tableName?cap_first}Service.delete(id);
        return ResponseEntity.ok().build();
    }

}
