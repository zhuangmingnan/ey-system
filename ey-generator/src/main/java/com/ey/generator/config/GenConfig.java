package com.ey.generator.config;

import lombok.Data;

/**
 * @author zhuang_ming_nan
 * @since 2019/9/27 18:47
 */
@Data
public class GenConfig {

    private String author;

}
