package com.ey.system.admin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
*
* @author ${author!"author"}
* @since ${.now?date} ${.now?time}
*/
@RestController
public class ${className?cap_first}Controller {

    @GetMapping("/health.do")
    public String health() {
        return "It's ey-system-admin running.";
    }
}
